//
//  ListOfImagesTableViewCell.swift
//  DesafioIOS
//
//  Created by Andre Vieira on 14/05/15.
//  Copyright (c) 2015 andre. All rights reserved.
//
//  Classe que representa a cell da UITableView
//  possui a imagem que será mostrada na lista
//  e a quantidade de visualizações feitas no site
//

import UIKit

class ListOfImagesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var ImageView: UIImageView!
    @IBOutlet weak var lbViews: UILabel!
    

}
