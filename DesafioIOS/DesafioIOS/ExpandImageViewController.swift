//
//  ExpandImageViewController.swift
//  DesafioIOS
//
//  Created by Andre Vieira on 14/05/15.
//  Copyright (c) 2015 andre. All rights reserved.
//
//  Controller da abertura da imagem com as informações
//  do usuário, descrição da imagem e a própria imagem
//

import UIKit

class ExpandImageViewController: UIViewController {
    
    @IBOutlet weak var Image: UIImageView!
    @IBOutlet weak var lbUsername: UILabel!
    @IBOutlet weak var webViewDescriptionOfImage: UIWebView!
    @IBOutlet weak var imageUser: UIImageView!
    
    var shot: Shots!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Image.image = UIImage(data: NSData(contentsOfURL: NSURL(string: shot.bigImageUrl)!)!)
        imageUser.image = UIImage(data: NSData(contentsOfURL: NSURL(string: shot.vatarUrl)!)!)
        lbUsername.text = shot.username
        webViewDescriptionOfImage.loadHTMLString(shot.description, baseURL: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
