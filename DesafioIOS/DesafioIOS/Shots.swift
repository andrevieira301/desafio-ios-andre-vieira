//
//  Shots.swift
//  DesafioIOS
//
//  Created by Andre Vieira on 14/05/15.
//  Copyright (c) 2015 andre. All rights reserved.
//
//  Classe representa o Shot, que nada mais é do que
//  um conjuntos com todas as informações sobre a imagem
//  como usuário, quantidade de visualizações e URLs da imagem
//
//
//

import Foundation


class Shots{
    var description: String!
    var viewsCount: Int!
    var title: String!
    var smallImageUrl: String!
    var bigImageUrl: String!
    var username: String!
    var vatarUrl: String!
    
    init(data: Dictionary<String, AnyObject>){
        
        self.description = self.getStringFromJson(data, key: "description")
        self.viewsCount = self.getIntFromJson(data, key: "views_count")
        self.title = self.getStringFromJson(data, key: "title")
        self.smallImageUrl = self.getStringFromJson(data, key: "image_teaser_url")
        
        self.bigImageUrl = self.getStringFromJson(data, key: "image_url")
        
        if let player = data["player"] as? Dictionary<String, AnyObject> {
            self.username = self.getStringFromJson(player, key: "username")
            self.vatarUrl = self.getStringFromJson(player, key: "avatar_url")
        }
        
    }
    
    /// verifica se o que veio do json pode ser uma string e o retorna
    /// caso não possa ser convertido retorna string vazia
    private func getStringFromJson(data: NSDictionary, key: String) -> String {
        
        if let info = data[key] as? String {
            return info
        }
        else {
            return ""
        }
        
    }
    
    /// verifica se o que veio do json pode ser um Int e o retorna
    /// caso não possa ser convertido retorna nil
    private func getIntFromJson(data: NSDictionary, key: String) -> Int? {
        
        if let info = data[key] as? Int {
            return info
        }
        else{
            return nil
        }
    }
}
