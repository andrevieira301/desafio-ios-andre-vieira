//
//  ViewController.swift
//  DesafioIOS
//
//  Created by Andre Vieira on 14/05/15.
//  Copyright (c) 2015 andre. All rights reserved.
//

import UIKit

class ViewController: UIViewController,  UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!

    var parametro = ["page": 1]
    var nextPage: Int = 1 // numero da pagina corrente
    var numPages: Int = 1000 // numero de paginas restante
    var shots = Array<Shots>()
    
    let expandImageSegueIdentifier = "ShowExpendImage"
    let urlResquest = "http://api.dribbble.com/shots/popular" //URL Dribbble
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        self.getTheListOfImageOfPage()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shots.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("CellImage", forIndexPath: indexPath) as! ListOfImagesTableViewCell
        
        var image = NSData(contentsOfURL: NSURL(string: shots[indexPath.row].bigImageUrl)!)
        
        cell.ImageView.image = UIImage(data: image!)
        cell.lbViews.text = "Views: \(shots[indexPath.row].viewsCount)"
        
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == expandImageSegueIdentifier {
            if let destination = segue.destinationViewController as? ExpandImageViewController{
                if let imageIndex = tableView.indexPathForSelectedRow()?.row {
                    destination.shot = shots[imageIndex]
                }
            }
        }
    }
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        // View para o botão de mais, que é add no rodapé da tela
        var footer = UIView(frame: CGRectMake(0, 0, 536, 50))
        footer.backgroundColor = UIColor(red: 243.0/255, green: 243.0/255, blue: 243.0/255, alpha: 1)
        var button = UIButton(frame: CGRectMake(25, 0, 536, 50))
        
        // Botão Mais, responsável por executar o método que realiza o request no site do dribbble
        button.setTitle("Mais", forState: UIControlState.Normal)
        button.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        button.addTarget(self, action: Selector("getTheListOfImageOfPage"), forControlEvents: UIControlEvents.TouchUpInside)
        
        footer.addSubview(button)
        
        return footer
        
    }
    
    
    /// Responsável por realizar o request e pegar o response no site
    /// do dribbble bem como interpretar a estrutura do json e montar os shots
    func getTheListOfImageOfPage(){
        
        let manager = AFHTTPRequestOperationManager()
        
        if self.numPages > 0 {
            manager.GET(
                urlResquest,
                parameters: parametro,
                success: {(operation: AFHTTPRequestOperation!, responseObject: AnyObject!) in
                    var json = responseObject as! Dictionary<String, AnyObject>
                    
                    // captura array de shots do json
                    var shotJson = json["shots"] as! Array<AnyObject>
                    
                    //print json shots
                    //println("Number page: \(self.nextPage)")
                    //println("Number pages remainder: \(self.numPages)")
                    //println("Json request page full dribbble: \(json)")
                    //println("Json request page dribbble shots: \(shotJson)")
                    
                    // Cria os shots do json
                    for data in shotJson {
                        var shot = Shots(data: data as! Dictionary<String, AnyObject>)
                        self.shots.append(shot)
                    }
                    
                    // set próxima pagina
                    self.nextPage++
                    
                    // Cálcula de paginas restantes
                    self.numPages = json["pages"] as! Int - self.nextPage
                    self.parametro.updateValue(self.nextPage, forKey: "page")
                    
                    
                    
                    self.tableView.reloadData()
                    
                },
                failure: {(operation: AFHTTPRequestOperation!,
                    error: NSError!) in
                    println(error)
            })
        }
    }

}

